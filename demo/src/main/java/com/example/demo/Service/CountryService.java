package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Country;
import com.example.demo.Model.Region;


@Service
public class CountryService {

    @Autowired
    RegionService regionsService;

    // cách 1 , tạo 1 object và thêm vào 3 thuộc tính 
    Country VN = new Country("VietNam", "VN" , new ArrayList<Region>(){
        {
            add(new Region("82","Kon Tum"));
        }
    });

    // cách 2 tạo 1 object và thêm 2 thuộc tính , có thể gọi riêng biệt , từng country , từng region 
    Country JP = new Country("Japan", "JP");
    Country US = new Country("United States", "US");

    public ArrayList<Country> getCountryList() {
        ArrayList<Country> countryList = new ArrayList<>();
        
        VN.setRegions(regionsService.getRegionVN());
        JP.setRegions(regionsService.getRegionJP());
        US.setRegions(regionsService.getRegionUS());

        countryList.add(VN);
        countryList.add(JP);
        countryList.add(US);

        return countryList;
    }
}
